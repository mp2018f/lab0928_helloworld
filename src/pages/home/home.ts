import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  who: string;
  greeting: string;
  fruits: string[] = ["Apple", "Pear", "Banana"];

  constructor(public navCtrl: NavController) {
    this.who = "Minho";
    this.fruits.push("Strawberry");
    this.fruits.pop();
    this.fruits.push(this.fruits.shift());
    this.fruits.sort();

    for( var f of this.fruits ) {
      console.log(f);
    }
    for( var f in this.fruits ) {
      console.log(f);
      console.log(this.fruits[f]);
    }
  }

  buttonClicked() {
    alert("What's up");
  }

  keyPressed(event) {
    console.log(event.key);
  }
}
