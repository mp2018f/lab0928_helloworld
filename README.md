LAB Source code for Hello World on 9/28
=======================================

How to download this repository
-------------------------------

1. git clone https://mp2018f@bitbucket.org/mp2018f/lab0928_helloworld.git
   (This downloads all the source codes from the repository, but files in .gitignore are missing)
2. cd lab0928_helloworld
3. npm install
   (Run this to populate other files necessary for the ionic to run, based on the package.json)
4. ionic serve

ENJOY